<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>">




	<div class="node-inner">

	<div class="node-curs">

			<div id="imatge-curs"><?php print render($content['field_image']); ?>

			<span class="places-curs"> Queden <?php $slots = node_registration_event_count($node); $resta = $registration->cache['capacity'] - $slots; echo $resta; ?> places </span><br />
			<span class="places-maxim">Màxim <?php print $registration->cache['capacity']; ?> persones</span> <br />
			<div id="boto-curs"><a class="boto" href="#inscripcions"><?php print render($content['registration_link']); ?></a></div>
			</div>


	<div id="curs-top">
	<div id="dades-curs">

		<div id="dades-curs-data"><?php print render($content['field_classe_data'][0]['#markup']); ?></div>


		<div id="dades-curs-durada"><b>Durada:</b> <?php print render($content['field_classe_durada'][0]['#markup']); ?></div>
			<div id="formador-curs"><b>Formador/a:</b> <?php print render($content['field_classe_formador'][0]['#markup']); ?></div>
			<div id="lloc-curs"><?php print render($content['field_classe_lloc'][0]['#markup']); ?></div>


		</div> <!-- dades curs -->

	</div> <!-- curs-top -->

	<div id="dades-curs">


	<div id="descripcio-curs"><?php print render($content['field_description'][0]['#markup']); ?></div>
	<?php print render($content['field_classe_arxius']); ?>
	<?php print render($content['field_classe_link']); ?>


        <div id="troc-curs">
        <h3>Què pots aportar a canvi del curs?</h3>
        <div class="preu-curs-ecos">O paga <?php print $node->field_ecos['und'][0]['value']; ?> ECOS</div>
        <?php print render($content['field_classe_troc']); ?>
        </div>

        			<div id="share-curs">
			<span class='st_facebook_hcount' displayText='Facebook'></span>
<span class='st_twitter_hcount' displayText='Tweet'></span>
<span class='st_email_hcount' displayText='Email'></span>
</div>




        </div>
        <div style="float:left;width:100%;clear:both;"></div>
        <div id="comentaris-curs">
		<?php print render($content['comments']); ?>
	</div>


	</div> <!-- /node-inner -->
	</div>

</div> <!-- /node-->

