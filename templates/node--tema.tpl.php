<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>">


	<div class="node-inner">

	<div id="anunci">

		<div id="anunci-right">
			<?php if ($content['field_image']) : ?>
			<div id="image-ad"><div class="glossy">
			<div class="morphing-tinting">
			<?php print render($content['field_image']); ?>
			</div>
			</div>
			<?php endif; ?>
			</div>

		</div>

		<div id="anunci-left">

	</div>

	<div id="description-ad">
	<h1>Comunitat > <a href="<?php print $GLOBALS['base_url']; ?>/comunitat/forum/all">Fòrum</a> > <a href="<?php print $GLOBALS['base_url']; ?>/comunitat/forum/<?php print $node->field_category_forum['und'][0]['taxonomy_term']->tid; ?>">
	<?php print $node->field_category_forum['und'][0]['taxonomy_term']->name; ?>
	</a></h1>

	<h2><?php print $title; ?></h2>


	<?php print render($content['field_descripcio_coneixement']); ?>



<div id="author-box">

			<div id="author-ad">
				<?php print views_embed_view('autor_anunci', 'block'); ?>
			</div>

			<div id="author-contacte">
			<?php global $user; ?>
			<?php if ($user->uid) : ?>
			<div id="contacte-caixa">

				<div class="telefon">
				<?php
				$node_author = user_load($node->uid);
				if ($node_author->field_privacity_telefon['und'][0]['value']): ?>
				<?php print t('No visible'); ?>

				<?php else : ?>

				<?php
				$node_author = user_load($node->uid);
				print ($node_author->field_user_telefon['und'][0]['value']);
				?>

				<?php endif; ?>
				</div>

			<?php
				if ($url = privatemsg_get_link(array(user_load($node->uid)))) {
					print l(t('Contactar'), $url, array('attributes' => array('class' => 'botocomprar')));
				}
			?>
			</div>
			<?php endif; ?>
			</div>
			</div>
	</div>

	<div id="comentaris">
		<?php print render($content['comments']); ?>
	</div>




	</div></div>
	</div> <!-- /node-inner -->
</div> <!-- /node-->

